<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('home');
})->middleware('auth');

Route::get('/gallery', 'AvatarController@index')->name('profile')->middleware('auth');
Route::resource('avatar', 'AvatarController')->middleware('auth');
Route::get('images/delete/{id}', 'GalleryController@deleteImage')->name('delete.image')->middleware('auth');
Route::get('delete_all', 'AvatarController@delAll')->name('deleteAll')->middleware('auth');
Route::post('/import_pdf','ProductController@export_pdf')->name('exportPDF')->middleware('auth');
Route::get('display', [
    'uses' => 'GalleryController@display'
])->middleware('auth');
Auth::routes();

/*Route::get('/pdf','ProductController@export_pdf')->name('export_pdf');*/
//Route::get('/pdf_final','ProductController@export_pdf')->name('export.pdf');

/*Route::post('/import','ProductController@import_excel');*/


/*Route::get('importExport', 'ProductController@importExport');
Route::get('downloadExcel/{type}', 'ProductController@downloadExcel');
Route::post('importExcel', 'ProductController@importExcel');*/




Route::get('/home', 'HomeController@index')->name('home');
