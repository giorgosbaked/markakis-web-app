@extends('layouts.app')

@section('content')
<div class="container-fluid" style="max-width: 1600px; margin: 50px auto 50px auto; position: static;">
    <form action="{{ route('exportPDF') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="d-flex">
            <label style="width: 49%;">
                <div class="card-header d-flex justify-content-between align-items-center">
                    Ημερομηνία Εξωφύλλου
                    <input style="width: 60%; height: 35px; padding: 0 15px; border: 1px solid #ddd;" type="text" name="cover_date" id="cover_date" value="ΣΕΠΤΕΜΒΡΙΟΥ - ΟΚΤΩΒΡΙΟΥ 2019">
                </div>
            </label>
            <label style="width: 49%; margin-left: 20px;">
                <div class="card-header d-flex justify-content-between align-items-center" style="padding-bottom: 16px; padding-top: 20px;">
                    Έξτρα σελίδα οπισθόφυλλου
                    <input type="checkbox" name="extra_page" value="extra_page" checked="checked">
                </div>
            </label>
        </div>
        <div class="row">
            <div class="card col-sm-4 bg-light m-0 mt-3">
                <label>
                    <div class="card-header d-flex justify-content-between align-items-center">
                        Παραφάρμακα
                        <input type="checkbox" name="parafarmaka" value="parafarmaka" checked="checked">
                    </div>
                </label>
                <div class="card-body">
                    <label for="title1">Τίτλος</label>
                    <input style="width: 100%; height: 35px; padding: 0 15px; margin-bottom: 15px; border: 1px solid #ddd;" type="text" name="title1" id="title1" value="ΚΑΤΑΛΟΓΟΣ ΠΩΛΗΤΗ ΠΑΡΑΦΑΡΜΑΚΟΥ">
                    <label for="subtitle1">Υπότιτλος</label>
                    <input style="width: 100%; height: 35px; padding: 0 15px; margin-bottom: 15px; border: 1px solid #ddd;"type="text" name="subtitle1" id="subtitle1" value="ΣΕΠΤΕΜΒΡΙΟΥ - ΟΚΤΩΒΡΙΟΥ 2019">  
                    <textarea name="editor1" id="editor1" rows="10" cols="80">
                        <div style="text-align: center;">
                            <h3><strong><ins>ΠΕΡΙΕΧΟΜΕΝΑ</ins></strong></h3>
                            <br><br>
                            <h3><strong><ins>ΣΗΜΕΙΩΣΗ</ins></strong></h3><br>
                            <h3><strong>ΠΑΡΑΚΑΛΩ ΔΙΑΒΑΣΤΕ ΠΡΟΣΕΚΤΙΚΑ</strong></h3><br>
                        </div>
                        <b>Για την παρούσα προσφορά ισχύουν οι παρακάτω όροι:</b>
                        <br><br>
                        <ol>
                            <li>Το τεύχος αυτό συμπληρώνεται από εσάς και αποστέλλεται μέσω του δικτύου διανομής της αποθήκης πίσω σε μας ώστε να εκτελεστεί.</li>
                            <li>Η προσφορά εκτελείται και εξοφλείται ξεχωριστά από τις καθημερινές σας παραγγελίες.</li>
                            <li>Θα ενημερώνεστε πριν την αποστολή της προσφοράς σε εσάς για το συνολικό ποσό που αυτή ανέρχεται.</li>
                            <li>Η πληρωμή θα γίνεται με την παραλαβή της παραγγελίας.</li>
                            <li>Ελάχιστο ποσό παραγγελίας είναι τα 1000 €.</li>
                            <li>Δεν συσχετίζεται με τις υπόλοιπες προσφορές της φαρμακαποθήκης.</li>
                            <li>Μπορείτε να παραγγείλετε οποιεσδήποτε ποσότητες επιθυμείτε από κάθε προϊόν. Συμπληρώνετε την ποσότητα που επιθυμείτε στο κουτάκι δίπλα σε κάθε προϊόν.</li>
                            <li>Οι προσφορές ισχύουν κατόπιν ύπαρξης αποθέματος. Αν κάποιο από τα προϊόντα που παραγγείλατε έχει εξαντληθεί προσωρινά, θα ενημερώνεστε τηλεφωνικά από την αποθήκη μας ώστε να εξυπηρετηθείτε με τον καλύτερο δυνατό τρόπο.</li>
                            <li>Οι εκπτώσεις ακολουθούν την παρακάτω κλίμακα βάσει του
                                ΤΡΟΠΟΥ ΠΛΗΡΩΜΗΣ που εσείς θα επιλέξετε, ανεξάρτητα του ποσού που θα ανέρχεται η παραγγελία σας:
                                <ol>
                                    <li>Αν επιλέξετε να εξοφλήσετε το ποσό της προσφοράς τοις μετρητοίς,
                                    έχετε έκπτωση 5% επί του τιμολογίου της προσφοράς</li>
                                    <li>Αν επιλέξετε να εξοφλήσετε το ποσό της προσφοράς με
                                    επιταγή 60 ημερών, έχετε έκπτωση 4% επί του τιμολογίου της προσφοράς</li>
                                </ol>
                            </li>
                        </ol>
                    </textarea>              


                    <label style="margin-top:10px;">Αρχείο Excel</label>
                    <input style="border: unset; padding:0; outline: 1px solid #ccd4da;" type="file" name="file1" class="form-control"><br>
                    <label style="width: 100%;">
                        <div class="card-header d-flex justify-content-between align-items-center" style="padding-bottom: 16px; padding-top: 20px;">
                            Εμφάνιση Ποσοτήτων
                            <input type="checkbox" name="quantity1" value="quantity1">
                        </div>
                    </label>
                </div>
            </div>
            <div class="card col-sm-4 bg-light m-0 mt-3">
                <label>
                    <div class="card-header d-flex justify-content-between align-items-center">
                        OTC
                        <input type="checkbox" name="otc" value="otc" checked="checked">
                    </div>
                </label>
                <div class="card-body">
                    <label for="title2">Τίτλος</label>
                    <input style="width: 100%; height: 35px; padding: 0 15px; margin-bottom: 15px; border: 1px solid #ddd;" type="text" name="title2" id="title2" value="ΠΡΟΣΦΟΡΑ ΜΗΣΥΦΑ (OTC)">
                    <label for="subtitle2">Υπότιτλος</label>
                    <input style="width: 100%; height: 35px; padding: 0 15px; margin-bottom: 15px; border: 1px solid #ddd;" type="text" name="subtitle2" id="subtitle2" value="ΣΕΠΤΕΜΒΡΙΟΥ - ΟΚΤΩΒΡΙΟΥ 2019">                
                    <textarea name="editor2" id="editor2" rows="10" cols="80">
                        <div style="text-align: center;">
                            <h3><strong><ins>ΠΕΡΙΕΧΟΜΕΝΑ</ins></strong></h3>
                            <br><br>
                            <h3><strong><ins>ΣΗΜΕΙΩΣΗ</ins></strong></h3><br>
                            <h3><strong>ΠΑΡΑΚΑΛΩ ΔΙΑΒΑΣΤΕ ΠΡΟΣΕΚΤΙΚΑ</strong></h3><br>
                        </div>
                        <b>Για την παρούσα προσφορά ισχύουν οι παρακάτω όροι:</b>
                        <br><br>
                        <ol>
                            <li>Το τεύχος αυτό συμπληρώνεται από εσάς και αποστέλλεται μέσω του δικτύου διανομής της αποθήκης πίσω σε μας ώστε να εκτελεστεί.</li>
                            <li>Η προσφορά εκτελείται και εξοφλείται ξεχωριστά από τις καθημερινές σας παραγγελίες.</li>
                            <li>Θα ενημερώνεστε πριν την αποστολή της προσφοράς σε εσάς για το συνολικό ποσό που αυτή ανέρχεται.</li>
                            <li>Η πληρωμή θα γίνεται με την παραλαβή της παραγγελίας.</li>
                            <li>Ελάχιστο ποσό παραγγελίας είναι τα 1000 €.</li>
                            <li>Δεν συσχετίζεται με τις υπόλοιπες προσφορές της φαρμακαποθήκης.</li>
                            <li>Μπορείτε να παραγγείλετε οποιεσδήποτε ποσότητες επιθυμείτε από κάθε προϊόν. Συμπληρώνετε την ποσότητα που επιθυμείτε στο κουτάκι δίπλα σε κάθε προϊόν.</li>
                            <li>Οι προσφορές ισχύουν κατόπιν ύπαρξης αποθέματος. Αν κάποιο από τα προϊόντα που παραγγείλατε έχει εξαντληθεί προσωρινά, θα ενημερώνεστε τηλεφωνικά από την αποθήκη μας ώστε να εξυπηρετηθείτε με τον καλύτερο δυνατό τρόπο.</li>
                            <li>Οι εκπτώσεις ακολουθούν την παρακάτω κλίμακα βάσει του
                                ΤΡΟΠΟΥ ΠΛΗΡΩΜΗΣ που εσείς θα επιλέξετε, ανεξάρτητα του ποσού που θα ανέρχεται η παραγγελία σας:
                                <ol>
                                    <li>Αν επιλέξετε να εξοφλήσετε το ποσό της προσφοράς τοις μετρητοίς,
                                    έχετε έκπτωση 5% επί του τιμολογίου της προσφοράς</li>
                                    <li>Αν επιλέξετε να εξοφλήσετε το ποσό της προσφοράς με
                                    επιταγή 60 ημερών, έχετε έκπτωση 4% επί του τιμολογίου της προσφοράς</li>
                                </ol>
                            </li>
                        </ol>
                    </textarea>  
                    <label style="margin-top:10px;">Αρχείο Excel</label>
                    <input style="border: unset; padding:0; outline: 1px solid #ccd4da;" type="file" name="file2" class="form-control"><br>
                    <label style="width: 100%;">
                        <div class="card-header d-flex justify-content-between align-items-center" style="padding-bottom: 16px; padding-top: 20px;">
                            Εμφάνιση Ποσοτήτων
                            <input type="checkbox" name="quantity2" value="quantity2">
                        </div>
                    </label>
                </div>
            </div>
            <div class="card col-sm-4 bg-light m-0 mt-3">
                <label>
                    <div class="card-header d-flex justify-content-between align-items-center">
                        Φάρμακα 
                        <input type="checkbox" name="farmaka" value="farmaka" checked="checked">
                    </div>
                </label>
                <div class="card-body">
                    <label for="title3">Τίτλος</label>
                    <input style="width: 100%; height: 35px; padding: 0 15px; margin-bottom: 15px; border: 1px solid #ddd;" type="text" name="title3" id="title3" value="ΠΡΟΣΦΟΡΑ ΦΑΡΜΑΚΟΥ">
                    <label for="subtitle3">Υπότιτλος</label>
                    <input style="width: 100%; height: 35px; padding: 0 15px; margin-bottom: 15px; border: 1px solid #ddd;" type="text" name="subtitle3" id="subtitle3" value="ΣΕΠΤΕΜΒΡΙΟΥ - ΟΚΤΩΒΡΙΟΥ 2019"> 
                    <textarea name="editor3" id="editor3" rows="10" cols="80">
                        <div style="text-align: center;">
                            <h3><strong><ins>ΠΕΡΙΕΧΟΜΕΝΑ</ins></strong></h3>
                            <br><br>
                            <h3><strong><ins>ΣΗΜΕΙΩΣΗ</ins></strong></h3><br>
                            <h3><strong>ΠΑΡΑΚΑΛΩ ΔΙΑΒΑΣΤΕ ΠΡΟΣΕΚΤΙΚΑ</strong></h3><br>
                        </div>
                        <b>Για την παρούσα προσφορά ισχύουν οι παρακάτω όροι:</b>
                        <br><br>
                        <ol>
                            <li>Το τεύχος αυτό συμπληρώνεται από εσάς και αποστέλλεται μέσω του δικτύου διανομής της αποθήκης πίσω σε μας ώστε να εκτελεστεί.</li>
                            <li>Η προσφορά εκτελείται και εξοφλείται ξεχωριστά από τις καθημερινές σας παραγγελίες.</li>
                            <li>Θα ενημερώνεστε πριν την αποστολή της προσφοράς σε εσάς για το συνολικό ποσό που αυτή ανέρχεται.</li>
                            <li>Η πληρωμή θα γίνεται με την παραλαβή της παραγγελίας.</li>
                            <li>Ελάχιστο ποσό παραγγελίας είναι τα 1000 €.</li>
                            <li>Δεν συσχετίζεται με τις υπόλοιπες προσφορές της φαρμακαποθήκης.</li>
                            <li>Μπορείτε να παραγγείλετε οποιεσδήποτε ποσότητες επιθυμείτε από κάθε προϊόν. Συμπληρώνετε την ποσότητα που επιθυμείτε στο κουτάκι δίπλα σε κάθε προϊόν.</li>
                            <li>Οι προσφορές ισχύουν κατόπιν ύπαρξης αποθέματος. Αν κάποιο από τα προϊόντα που παραγγείλατε έχει εξαντληθεί προσωρινά, θα ενημερώνεστε τηλεφωνικά από την αποθήκη μας ώστε να εξυπηρετηθείτε με τον καλύτερο δυνατό τρόπο.</li>
                            <li>Οι εκπτώσεις ακολουθούν την παρακάτω κλίμακα βάσει του
                                ΤΡΟΠΟΥ ΠΛΗΡΩΜΗΣ που εσείς θα επιλέξετε, ανεξάρτητα του ποσού που θα ανέρχεται η παραγγελία σας:
                                <ol>
                                    <li>Αν επιλέξετε να εξοφλήσετε το ποσό της προσφοράς τοις μετρητοίς,
                                    έχετε έκπτωση 5% επί του τιμολογίου της προσφοράς</li>
                                    <li>Αν επιλέξετε να εξοφλήσετε το ποσό της προσφοράς με
                                    επιταγή 60 ημερών, έχετε έκπτωση 4% επί του τιμολογίου της προσφοράς</li>
                                </ol>
                            </li>
                        </ol>
                    </textarea>                 
                    <label style="margin-top:10px;">Αρχείο Excel</label>
                    <input style="border: unset; padding:0; outline: 1px solid #ccd4da;" type="file" name="file3" class="form-control"><br>
                    <label style="width: 100%;">
                        <div class="card-header d-flex justify-content-between align-items-center" style="padding-bottom: 16px; padding-top: 20px;">
                            Εμφάνιση Ποσοτήτων
                            <input type="checkbox" name="quantity3" value="quantity3">
                        </div>
                    </label>
                </div>
            </div>
        </div>
        <br>
        <button class="btn btn-success">Import Data</button>
    </form>
</div>
@endsection