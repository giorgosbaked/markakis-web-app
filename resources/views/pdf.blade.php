<!DOCTYPE html>
<html lang="el">
<head>
  <title>ΜΑΡΚΑΚΗ</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
  <style>
    @page {
      margin: 5mm;
      padding: 8mm;
    }
    body {
      font-family: DejaVu Sans Mono, sans-serif !important; 
      font-size: 14px;
    }

    .border-unset {
      border-bottom: unset;
      position: relative;
    }

    .border-unset span {
      position: absolute;
    }

    h1 {
      text-align: center;
    }

    .spacer {
      height: 60px;
    }
    .hide {
      display: none;
    }

    .main {
      margin:15mm;
    }

    #content {
      position:relative;
      display:table;
      table-layout:fixed;
      padding-top:20px;
      padding-bottom:20px;
      width: 100%;
      height:auto;
    }

    .generic-section {
      break-after: always;
      height: 1664px;
    }
    
    .editor *:not(li):not(ol):not(ul) {
      text-align: center;
    }

    li {
      font-size: 20px;
    }

    .ads {
      position: relative;
      height: 100px;
      border: unset;
    }

    .logos {
      position: relative;
    }

    .logos td {
      height: 64px;
      border: unset;
      max-width: 60px;
      padding-top: 20px;
    }

    .space_small td {
      border: unset;
      height: 32px;
    }

    .text td {
      border: unset;
      max-width: 60px;
      padding-bottom: 10px;
    }

    .logos hr:first-of-type {
      border-radius: 20px 0px 0px 20px;
    }

    .logos hr:last-of-type {
      border-radius: 0px 20px 20px 0px;
    }

    .ads img {
      max-height: 70px;
      position: absolute;
      top: 15px;
    }

    section {
      padding: 30px;
    }

    td, tr {
      padding:5px;
    }
    table {
      max-width: 2480px;
      width:100%;
    }

    .generic-section, .generic-section .container, .editor, .second-section {
      display: flex;
      justify-content: center;
      align-items: center;
      flex-direction: column;
    }

    .first_page {
      break-after: always;
      background:url('{{asset("assets/img/front.jpg")}}') no-repeat center;
      height: 1851px;
      background-size: contain;
    }

    .last_page {
      break-before: always;
      background:url('{{asset("assets/img/back.jpg")}}') no-repeat center;
      height: 1851px;
      background-size: contain;
    }

    #pre_last_page {
      break-before:always;
      width: 100%;
      text-align: center;
    }

    th, td {
      border: 1px solid black;
    }

    table.no-tem td:nth-child(3), table.no-tem th:nth-child(3), table.no-ekpt td:nth-child(4), table.no-ekpt th:nth-child(4), 
    table.no-xondr td:nth-child(5), table.no-xondr th:nth-child(5), table.no-posot td:nth-child(6), 
    table.no-posot th:nth-child(6)  {
      display: none;
    }

    thead { display: table-header-group; }
    tfoot { display: table-footer-group; }

    @media print {
      .hide {
        display: block;
      }
    }
  </style>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script>
    function remove($key, $class) {
      var element = document.getElementById("table" + $key);
      element.classList.add("no-" + $class);
    }
  </script>
</head>
<body>
  <div id="printableArea">
    <section class="first_page">
      <div style="display: flex; align-items: center; justify-content: center;flex-direction: column;margin-top: 670px;">
        <h1 style="font-family: Ubuntu; font-size: 45px; max-width:700px; margin:auto; font-weight: bold; color: #013f37; letter-spacing: 11px;">ΚΑΤΑΛΟΓΟΣ<br>ΠΡΟΣΦΟΡΩΝ</h1>
        <h2 style="font-family: Ubuntu; font-size: 25px; font-weight: 300; font-style: italic; color: #666666;margin-right:15px; margin-top: 20px;">{{ $input['cover_date'] }}</h2>
      </div>
    </section>
    <table style="width: calc(100% - 30px ); margin-left: 30px;">
     <thead class="first_header">
      <tr>
        <td style="border: unset;">
          <div class="hide">
            <img src="{{asset('assets/img/headerpdf.png')}}" alt="header-img">
          </div>
        </td>
      </tr>
    </thead>
    <tbody style="break-after: always;">
      <tr><td style="border: unset;">
        <section id="content">
          @if ($array1)
          <section style="text-align:center; height: 1664px;">
            <img style="margin: 150px auto 250px auto;" width="250" src="{{asset('assets/logo.png')}}" alt="logo">
            <div style="background-color: #00735d; padding:50px 0;">
              <div style="background-color: #00735d; padding:50px 0;">
                <h1 style="font-family: Ubuntu; font-size: 50px; max-width:700px; margin:auto; font-weight: bold; color: #fff; letter-spacing: 11px;">{{ $input['title1'] }}</h1>
                <h2 style="font-family: Ubuntu; font-size: 30px; font-weight: 300; font-style: italic; color: #fff; margin-top: 20px;">{{ $input['subtitle1'] }}</h2>
              </div>
            </div>
          </section>

          <section class="generic-section">
            <div class="container">
              <div class="editor">
                {!! $input['editor1']!!}
              </div>
            </div>
          </section>

          <table id="table1" style="break-after: always;">
            <tbody>
              @php
              $temp1 = 0;
              $temp2 = 0;
              $temp3 = 0;
              $temp4 = 0;
              @endphp
              @foreach ($array1 as $row)
              @foreach ($row as $k => $r)
              @if ($r['kodikos'] == 'space1')
              <tr class="space_small"><td></td><td></td></tr>
              @continue
              @elseif ($r['kodikos'] == 'space')
              <tr class="logos"><td></td><td><div class="spacer"></div></td></tr>
              @continue
              @elseif ($r['kodikos'] == 'brand') 
              <tr class="logos"><td></td><td><span style="color: #016b5b; font-size: 30px; font-weight:bold; margin-left: -88px;">{{$r['eidos']}}</span></td>
                <td></td><td></td>
                <td>
                  <img style="position: absolute; right:0;" src="{{asset('assets/img/logo_bg.png')}}" alt="logo-bg">
                </td>
              </tr>
              @if ($row[$k+1]['kodikos'] != 'text')
              <tr>
                <td><b>Κωδικός</b></td>
                <td><b>Είδος</b></td>
                <td><b>Τεμάχια</b></td>
                <td><b>Έκπτωση</b></td>
                <td><b>Χονδρική τιμή</b></td>
                <td><b>Ποσότητα</b></td>
              </tr>
              @endif
              @continue
              @elseif ($r['kodikos'] == 'ad')
              <tr><td class="ads"><img src="{{ asset('storage/general/' . $r['eidos'])}}"></td></tr>
              @continue
              @elseif ($r['kodikos'] == 'text')
              <tr class="text"><td></td><td><span style="font-size: 15px; font-style: italic;"><b>{{$r['eidos']}}</b></span></td></tr>
              @if ($row[$k-1]['kodikos'] == 'brand')
              <tr>
                <td><b>Κωδικός</b></td>
                <td><b>Είδος</b></td>
                <td><b>Τεμάχια</b></td>
                <td><b>Έκπτωση</b></td>
                <td><b>Χονδρική τιμή</b></td>
                <td><b>Ποσότητα</b></td>
              </tr>
              @endif
              @continue
              @endif
              <tr>
                {{-- Row with children --}}
                @if (!(empty($r['kodikos'])) && empty($row[$loop->index+1]['kodikos']))
                <?php 
                $k=count($row); 
                $count=0;
                ?>
                @for ($i = 1; $i < $k-$loop->index; $i++)
                @if (empty($row[$loop->index+$i]['kodikos']))
                <?php $count++; ?>
                @else 
                @break;
                @endif
                @endfor
                @if (empty($row[$loop->index+$count]['kodikos'] && !$loop->last))
                <td rowspan="{{$count+1}}"><span>{{ $r['kodikos'] }} </span></td>
                <td rowspan="{{$count+1}}"><span>{{ $r['eidos'] }} </span></td>
                @endif
                {{-- Row without children --}}
                @elseif (!empty($r['kodikos']) && !empty($row[$loop->index+1]) && !$loop->last) 
                <td>{{ $r['kodikos'] }} </td>
                <td>{{ $r['eidos'] }} </td>
                @else
                <td style="display: none;"></td><td style="display: none;"></td>
                @endif
                <td>{{ $r['temaxia'] }} </td>
                <td>@if ($r['ekptosi'] > 0 && $r['ekptosi'] <= 1) 
                  {{$r['ekptosi']*100 }}%
                  @else 
                  {{ $r['ekptosi'] }}
                  @endif
                </td>
                <td>{{ $r['xondriki_timi'] }}@if (!empty($r['xondriki_timi']))&euro; @endif</td>
                <td>{{ $r['posotita'] }} </td>
              </tr>
              @php
              if (!empty($r['temaxia']))
                $temp1 = 1;
              if (!empty($r['ekptosi']))
                $temp2 = 1;
              if (!empty($r['xondriki_timi']))
                $temp3 = 1;
              if (!empty($r['posotita']))
                $temp4 = 1;
              @endphp
              @endforeach
              @endforeach
            </tbody>
          </table>
          @if ($temp1 == 0) 
          {!!'<script>remove(1,"tem");</script>'!!}
          @endif
          @if ($temp2 == 0) 
          {!!'<script>remove(1,"ekpt");</script>'!!}
          @endif
          @if ($temp3 == 0) 
          {!!'<script>remove(1,"xondr");</script>'!!}
          @endif
          @if ($temp4 == 0 && !(@isset($input['quantity1']))) 
          {!!'<script>remove(1,"posot");</script>'!!}
          @endif
          @endif

          @if ($array2)
          <section style="text-align:center; height: 1664px;">
            <img style="margin: 150px auto 250px auto;" width="250" src="{{asset('assets/logo.png')}}" alt="logo">
            <div style="background-color: #00735d; padding:50px 0;">
              <h1 style="font-family: Ubuntu; font-size: 50px; max-width:700px; margin:auto; font-weight: bold; color: #fff; letter-spacing: 11px;">{{ $input['title2'] }}</h1>
              <h2 style="font-family: Ubuntu; font-size: 30px; font-weight: 300; font-style: italic; color: #fff; margin-top: 20px;">{{ $input['subtitle2'] }}</h2>
            </div>
          </section>

          <section class="generic-section">
            <div class="container">
              <div class="editor">
                {!! $input['editor2']!!}
              </div>
            </div>
          </section>

          @php
          $temp1 = 0;
          $temp2 = 0;
          $temp3 = 0;
          $temp4 = 0;
          @endphp
          <table id="table2" style="break-after: always;">
            <tbody>
              @foreach ($array2 as $row)
              @foreach ($row as $k => $r)
              @if ($r['kodikos'] == 'space1')
              <tr class="space_small"><td></td><td></td></tr>
              @continue
              @elseif ($r['kodikos'] == 'space')
              <tr class="logos"><td></td><td><div class="spacer"></div></td></tr>
              @continue
              @elseif ($r['kodikos'] == 'brand') 
              <tr class="logos"><td></td><td><span style="color: #016b5b; font-size: 30px; font-weight:bold; margin-left: -88px;">{{$r['eidos']}}</span></td>
                <td></td><td></td>
                <td>
                  <img style="position: absolute; right:0;" src="{{asset('assets/img/logo_bg.png')}}" alt="logo-bg">
                </td>
              </tr>
              @if ($row[$k+1]['kodikos'] != 'text')
              <tr>
                <td><b>Κωδικός</b></td>
                <td><b>Είδος</b></td>
                <td><b>Τεμάχια</b></td>
                <td><b>Έκπτωση</b></td>
                <td><b>Χονδρική τιμή</b></td>
                <td><b>Ποσότητα</b></td>
              </tr>
              @endif
              @continue
              @elseif ($r['kodikos'] == 'ad')
              <tr><td class="ads"><img src="{{ asset('storage/general/' . $r['eidos'])}}"></td></tr>
              @continue
              @elseif ($r['kodikos'] == 'text')
              <tr class="text"><td></td><td><span style="font-size: 15px; font-style: italic;"><b>{{$r['eidos']}}</b></span></td></tr>
              @if ($row[$k-1]['kodikos'] == 'brand')
              <tr>
                <td><b>Κωδικός</b></td>
                <td><b>Είδος</b></td>
                <td><b>Τεμάχια</b></td>
                <td><b>Έκπτωση</b></td>
                <td><b>Χονδρική τιμή</b></td>
                <td><b>Ποσότητα</b></td>
              </tr>
              @endif
              @continue
              @endif     
              <tr>
                {{-- Row with children --}}
                @if (!(empty($r['kodikos'])) && empty($row[$loop->index+1]['kodikos']))
                <?php 
                $k=count($row); 
                $count=0;
                ?>
                @for ($i = 1; $i < $k-$loop->index; $i++)
                @if (empty($row[$loop->index+$i]['kodikos']))
                <?php $count++; ?>
                @else 
                @break;
                @endif
                @endfor
                @if (empty($row[$loop->index+$count]['kodikos'] && !$loop->last))
                <td rowspan="{{$count+1}}"><span>{{ $r['kodikos'] }} </span></td>
                <td rowspan="{{$count+1}}"><span>{{ $r['eidos'] }} </span></td>
                @endif
                {{-- Row without children --}}
                @elseif (!empty($r['kodikos']) && !empty($row[$loop->index+1]) && !$loop->last) 
                <td>{{ $r['kodikos'] }} </td>
                <td>{{ $r['eidos'] }} </td>
                @else
                <td style="display: none;"></td><td style="display: none;"></td>
                @endif
                <td>{{ $r['temaxia'] }} </td>
                <td>@if ($r['ekptosi'] > 0 && $r['ekptosi'] <= 1) 
                  {{$r['ekptosi']*100 }}%
                  @else 
                  {{ $r['ekptosi'] }}
                  @endif
                </td>
                <td>{{ $r['xondriki_timi'] }}@if (!empty($r['xondriki_timi']))&euro; @endif</td>
                <td>{{ $r['posotita'] }} </td>
              </tr>
              @php
              if (!empty($r['temaxia']))
                $temp1 = 1;
              if (!empty($r['ekptosi']))
                $temp2 = 1;
              if (!empty($r['xondriki_timi']))
                $temp3 = 1;
              if (!empty($r['posotita']))
                $temp4 = 1;
              @endphp
              @endforeach
              @endforeach
            </tbody>
          </table>
          @if ($temp1 == 0) 
          {!!'<script>remove(2,"tem");</script>'!!}
          @endif
          @if ($temp2 == 0) 
          {!!'<script>remove(2,"ekpt");</script>'!!}
          @endif
          @if ($temp3 == 0) 
          {!!'<script>remove(2,"xondr");</script>'!!}
          @endif
          @if ($temp4 == 0 && !(@isset($input['quantity2']))) 
          {!!'<script>remove(2,"posot");</script>'!!}
          @endif
          @endif

          @if ($array3)
          <section style="text-align:center; height: 1664px;">
            <img style="margin: 150px auto 250px auto;" width="250" src="{{asset('assets/logo.png')}}" alt="logo">
            <div style="background-color: #00735d; padding:50px 0;">
              <h1 style="font-family: Ubuntu; font-size: 50px; max-width:700px; margin:auto; font-weight: bold; color: #fff; letter-spacing: 11px;">{{ $input['title3'] }}</h1>
              <h2 style="font-family: Ubuntu; font-size: 30px; font-weight: 300; font-style: italic; color: #fff; margin-top: 20px;">{{ $input['subtitle3'] }}</h2>
            </div>
          </section>

          <section class="generic-section">
            <div class="container">
              <div class="editor">
                {!! $input['editor3']!!}
              </div>
            </div>
          </section>
          @php
          $temp1 = 0;
          $temp2 = 0;
          $temp3 = 0;
          $temp4 = 0;
          @endphp
          <table  id="table3" style="break-after: always;">
            <tbody>
              @foreach ($array3 as $row)
              @foreach ($row as $k => $r)
              @if ($r['kodikos'] == 'space1')
              <tr class="space_small"><td></td><td></td></tr>
              @continue
              @elseif ($r['kodikos'] == 'space')
              <tr class="logos"><td></td><td><div class="spacer"></div></td></tr>
              @continue
              @elseif ($r['kodikos'] == 'brand') 
              <tr class="logos"><td></td><td><span style="color: #016b5b; font-size: 30px; font-weight:bold; margin-left: -88px;">{{$r['eidos']}}</span></td>
                <td></td><td></td>
                <td>
                  <img style="position: absolute; right:0;" src="{{asset('assets/img/logo_bg.png')}}" alt="logo-bg">
                </td>
              </tr>
              @if ($row[$k+1]['kodikos'] != 'text')
              <tr>
                <td><b>Κωδικός</b></td>
                <td><b>Είδος</b></td>
                <td><b>Τεμάχια</b></td>
                <td><b>Έκπτωση</b></td>
                <td><b>Χονδρική τιμή</b></td>
                <td><b>Ποσότητα</b></td>
              </tr>
              @endif
              @continue
              @elseif ($r['kodikos'] == 'ad')
              <tr><td class="ads"><img src="{{ asset('storage/general/' . $r['eidos'])}}"></td></tr>
              @continue
              @elseif ($r['kodikos'] == 'text')
              <tr class="text"><td></td><td><span style="font-size: 15px; font-style: italic;"><b>{{$r['eidos']}}</b></span></td></tr>
              @if ($row[$k-1]['kodikos'] == 'brand')
              <tr>
                <td><b>Κωδικός</b></td>
                <td><b>Είδος</b></td>
                <td><b>Τεμάχια</b></td>
                <td><b>Έκπτωση</b></td>
                <td><b>Χονδρική τιμή</b></td>
                <td><b>Ποσότητα</b></td>
              </tr>
              @endif
              @continue
              @endif     
              <tr>
                {{-- Row with children --}}
                @if (!(empty($r['kodikos'])) && empty($row[$loop->index+1]['kodikos']))
                <?php 
                $k=count($row); 
                $count=0;
                ?>
                @for ($i = 1; $i < $k-$loop->index; $i++)
                @if (empty($row[$loop->index+$i]['kodikos']))
                <?php $count++; ?>
                @else 
                @break;
                @endif
                @endfor
                @if (empty($row[$loop->index+$count]['kodikos'] && !$loop->last))
                <td rowspan="{{$count+1}}"><span>{{ $r['kodikos'] }} </span></td>
                <td rowspan="{{$count+1}}"><span>{{ $r['eidos'] }} </span></td>
                @endif
                {{-- Row without children --}}
                @elseif (!empty($r['kodikos']) && !empty($row[$loop->index+1]) && !$loop->last) 
                <td>{{ $r['kodikos'] }} </td>
                <td>{{ $r['eidos'] }} </td>
                @else
                <td style="display: none;"></td><td style="display: none;"></td>
                @endif
                <td>{{ $r['temaxia'] }} </td>
                <td>@if ($r['ekptosi'] > 0 && $r['ekptosi'] <= 1) 
                  {{$r['ekptosi']*100 }}%
                  @else 
                  {{ $r['ekptosi'] }}
                  @endif
                </td>
                <td>{{ $r['xondriki_timi'] }}@if (!empty($r['xondriki_timi']))&euro; @endif</td>
                <td>{{ $r['posotita'] }} </td>
              </tr>
              @php
              if (!empty($r['temaxia']))
                $temp1 = 1;
              if (!empty($r['ekptosi']))
                $temp2 = 1;
              if (!empty($r['xondriki_timi']))
                $temp3 = 1;
              if (!empty($r['posotita']))
                $temp4 = 1;
              @endphp
              @endforeach
              @endforeach
            </tbody>
          </table>
          @if ($temp1 == 0) 
          {!!'<script>remove(3,"tem");</script>'!!}
          @endif
          @if ($temp2 == 0) 
          {!!'<script>remove(3,"ekpt");</script>'!!}
          @endif
          @if ($temp3 == 0) 
          {!!'<script>remove(3,"xondr");</script>'!!}
          @endif
          @if ($temp4 == 0 && !(@isset($input['quantity3']))) 
          {!!'<script>remove(3,"posot");</script>'!!}
          @endif
          @endif
        </section>
      </td></tr>
    </tbody>
    <tfoot >
      <tr>
        <td style="border: unset;">
          <div class="hide">
            <img src="{{asset('assets/img/footerpdf.png')}}" alt="footer-img">
          </div>
        </td>
      </tr>
    </tfoot>
  </table>
  @if (@isset($input['extra_page']) )
  <div id="pre_last_page">
    <img src="{{asset('assets/img/headerpdf.png')}}" alt="footer-img">
    <img src="{{asset('assets/img/footerpdf.png')}}" alt="footer-img">
  </div>
  @endif
  <section class="last_page" style="break-before: always;">
  </section>
</div>
</body>
</html>