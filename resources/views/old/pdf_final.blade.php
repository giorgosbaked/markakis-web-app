<!DOCTYPE html>
<html lang="el">
<head>
    <title>ΜΑΡΚΑΚΗ</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <style>
        @page { margin:15px; }
        body {
            font-family: DejaVu Sans Mono, sans-serif !important; 
        }
        td, tr {
            padding:5px;
        }
        table {
          border-collapse: collapse;
        }

        table, th, td {
          border: 1px solid black;
        }
    </style>
</head>
<body>
<table>
    <thead>
    <tr>
        <th>Κωδικός</th>
        <th>Είδος</th>
        <th>Τεμάχια</th>
        <th>Έκπτωση</th>
        <th>Χονδρική Τιμή</th>
        <th>Ποσότητα</th>
    </tr>
    </thead>
    <tbody>
    
    @foreach ($array as $row)
        @foreach ($row as $r)
            <tr>
                @if ($r['kodikos'] == 'logo') 
                {{-- <td style="border-left: unset!important;"></td> --}}
                <td style="position: relative; height: 60px; border: unset;"><img style="position: absolute; top: 15px;" width="100" src="{{ asset('assets/logos/durex.png') }}"></td>
            @else
                @if (empty($r['kodikos']) && empty($row[$loop->index+1]['kodikos']) && (!$loop->last))
                  <td style="border-bottom: unset; border-top:unset;">{{ $r['kodikos'] }} </td>
                  <td style="border-bottom: unset; border-top:unset;">{{ $r['eidos'] }} </td>
                @elseif (empty($row[$loop->index+1]['kodikos']) && (!$loop->last))
                  <td style="border-bottom: unset; position: relative;"><span style="position: absolute; top:15px;">{{ $r['kodikos'] }} </span></td>
                  <td style="border-bottom: unset; position: relative;"><span style="position: absolute; top:15px;">{{ $r['eidos'] }} </span></td>
                @elseif (empty($r['kodikos']))
                  <td style="border-top: unset;">{{ $r['kodikos'] }} </td>
                  <td style="border-top: unset;">{{ $r['eidos'] }} </td>
                @else
                  <td>{{ $r['kodikos'] }} </td>
                  <td>{{ $r['eidos'] }} </td>
                @endif


                <td>{{ $r['temaxia'] }} </td>
                <td>{{ $r['ekptosi'] }} </td>
                <td>{{ $r['xondriki_timi'] }}&euro; </td>
                <td>{{ $r['posotita'] }} </td>
            </tr>
            @endif
        @endforeach
    @endforeach

    </tbody>
</table>
{{-- <div style="height: 932px; text-align: center; border:1px solid red;">
    <img width="250" src="{{asset('assets/logo.png')}}" alt="logo">
    <h1>{{ $input['title'] }}</h1>
    <h2>{{ $input['subtitle'] }}</h2>
    <img src="{{ asset('assets/img/'.$originalFile) }}" alt="main">
</div> --}}
</body>
</html>