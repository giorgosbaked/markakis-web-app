<!DOCTYPE html>
<html lang="el">
<head>
    <title>ΜΑΡΚΑΚΗ</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <style>
        @page {
          margin: 15px;
        }
        body {
            font-family: DejaVu Sans Mono, sans-serif !important; 
            font-size: 14px;
        }
        td, tr {
            padding:5px;
        }
        table {
          border: 1px solid transparent;
          border-collapse: collapse;
        }

        th, td {
          border: 1px solid black;
        }

        .border-unset {
          border-bottom: unset;
          position: relative;
        }

        .border-unset span {
          position: absolute;
        }
    </style>
</head>
<body>
<div style="page-break-after: always; height: 990px; text-align: center; border:1px solid red;">
    <img width="250" src="{{asset('assets/logo.png')}}" alt="logo">
    <h1>{{ $input['title1'] }}</h1>
    <h2>{{ $input['subtitle1'] }}</h2>
    <img src="{{ asset('assets/img/'.$originalFile) }}" alt="main">
</div>

<table>
  <thead >
    <tr>
      <th>Κωδικός</th>
      <th>Είδος</th>
      <th>Τεμάχια</th>
      <th>Έκπτωση</th>
      <th>Χονδρική τιμή</th>
      <th>Ποσότητα</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($array1 as $row)
        @foreach ($row as $r)
            
            @if ($r['kodikos'] == 'logo') 
              <tr><td style="position: relative; height: 60px; border: unset;"><img style="position: absolute; top: 15px;" width="100" src="{{ asset('assets/logos/durex.png') }}"></td></tr>
              @continue
            @endif            
            <tr>
                @if (empty($r['kodikos']) && empty($row[$loop->index+1]['kodikos']) && (!$loop->last))
                  <td style="border-bottom: unset; border-top:unset;">{{ $r['kodikos'] }} </td>
                  <td style="border-bottom: unset; border-top:unset;">{{ $r['eidos'] }} </td>
                @elseif (empty($row[$loop->index+1]['kodikos']) && ($loop->index+1 != $loop->last))
                  <?php 
                  $test=count($row); 
                  $count=0;
                  ?>
                  {{-- <td>New count{{ $count }}</td> --}}
                  @for ($i = 1; $i < $test-$loop->index; $i++)
                    @if (empty($row[$loop->index+$i]['kodikos']))
                    <?php $count++; ?>
                    @else 
                      @break;
                    @endif
                  @endfor
                  @if (empty($row[$loop->index+$count]['kodikos']))
                    <td class="border-unset"><span style="top:{{$count*18}}px">{{ $r['kodikos'] }} </span></td>
                    <td class="border-unset"><span style="top:{{$count*18}}px">{{ $r['eidos'] }} </span></td>
                  @endif
                  
                @elseif (empty($r['kodikos']))
                  <td style="border-top: unset;">{{ $r['kodikos'] }} </td>
                  <td style="border-top: unset;">{{ $r['eidos'] }} </td>
                @else
                  <td>{{ $r['kodikos'] }} </td>
                  <td>{{ $r['eidos'] }} </td>
                @endif


                <td>{{ $r['temaxia'] }} </td>
                <td>{{ $r['ekptosi'] }} </td>
                <td>{{ $r['xondriki_timi'] }}
                  @if (!empty($r['xondriki_timi']))
                    &euro; 
                  @endif
                  </td>
                <td>{{ $r['posotita'] }} </td>
            </tr>
            
        @endforeach
    @endforeach
      {{-- <a href="{{ route('export.pdf') }}">Generate PDF!</a> --}}
    </tbody>
</table>

</body>
</html>