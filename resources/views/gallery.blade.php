@extends('layouts.app')

@section('content')
@if (Auth::check())
<div class="container">
	<div class="row">
		<form action="{{route('avatar.store')}}" method="post" enctype="multipart/form-data">
			@csrf

			<div class="input-group mb-3">
				<div class="custom-file">
					<input type="file" name="avatar[]" class="custom-file-input" id="inputGroupFile01" multiple>
					<label class="custom-file-label" for="inputGroupFile01">Choose file</label>

				</div>
				<input type="submit" value="Upload" class="btn btn-success ml-4">
			</div>
		</form>
		<p id="demo"></p>
	</div>
</div>
@endif

{!! Form::open(array('action' => 'GalleryController@display','method' => 'GET'))!!}
@csrf
<div class="container-fluid">
	<div class="row">
		@foreach($avatars as $key => $avatar)
		<div class="card mr-2 mb-2" style="width: 300px!important; padding:5px;">
			<div class="form-group mb-3">
				<input type="checkbox" name="agree[]" id="myCheckbox{{$key}}" value="{{$avatar->id}}" />
				@if (Auth::check())
				<a href="{{route('delete.image', $loop->index)}}">&#10005;</a>
				@endif
				<label for="myCheckbox{{$key}}"><img class="card-img-top" src="{{$avatar->getFullUrl()}}" alt="Customer Logo">{{$avatar->file_name}}</label>
			</div>
		</div>
		@endforeach
	</div>
</div>
<div class="container-fluid mt-4">
	<div class="row">
		@if (Auth::check())
		<a class="btn btn-danger mr-3" href="{{route('deleteAll')}}">Delete All</a><br>
		@endif
	</div>
</div>
{!! Form::close() !!}

@endsection
<style>
	form { display: flex; flex-wrap: wrap; }
	.form-group a { float: right; color: red; font-size: 20px}
	.form-group img { cursor: pointer; }
	.row {margin-right: 30px!important; margin-left: 30px!important;}
</style>
<script>
function toggle(source) {
  checkboxes = document.getElementsByName('agree[]');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
</script> 