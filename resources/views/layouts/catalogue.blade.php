@extends('pdf')
@section('catalogue')
@foreach ($row as $k => $r)

              @if ($r['kodikos'] == 'space')
              <tr class="logos"><td></td><td><div class="spacer"></div></td></tr>
              @continue
              @elseif ($r['kodikos'] == 'brand') 
              <tr class="logos"><td></td><td><span style="color: #016b5b; font-size: 30px; font-weight:bold; margin-left: -88px;">{{$r['eidos']}}</span></td>
                <td></td><td></td>
                <td>
                  <img style="position: absolute; right:0;" src="{{asset('assets/img/logo_bg.png')}}" alt="logo-bg">
                </td>
              </tr>
              @if ($row[$k+1]['kodikos'] != 'text')
              <tr>
                <td><b>Κωδικός</b></td>
                <td><b>Είδος</b></td>
                <td><b>Τεμάχια</b></td>
                <td><b>Έκπτωση</b></td>
                <td><b>Χονδρική τιμή</b></td>
                <td><b>Ποσότητα</b></td>
              </tr>
              @endif
              @continue
              @elseif ($r['kodikos'] == 'ad')
              <tr><td class="ads"><img src="{{ asset('storage/general/' . $r['eidos'])}}"></td></tr>
              @continue
              @elseif ($r['kodikos'] == 'text')
              <tr class="text"><td></td><td><span style="font-size: 15px; font-style: italic;"><b>{{$r['eidos']}}</b></span></td></tr>
              @if ($row[$k-1]['kodikos'] == 'brand')
              <tr>
                <td><b>Κωδικός</b></td>
                <td><b>Είδος</b></td>
                <td><b>Τεμάχια</b></td>
                <td><b>Έκπτωση</b></td>
                <td><b>Χονδρική τιμή</b></td>
                <td><b>Ποσότητα</b></td>
              </tr>
              @endif
              @continue
              @endif
              <tr>
                {{-- Row with children --}}
                @if (!(empty($r['kodikos'])) && empty($row[$loop->index+1]['kodikos']))
                <?php 
                $k=count($row); 
                $count=0;
                ?>
                @for ($i = 1; $i < $k-$loop->index; $i++)
                @if (empty($row[$loop->index+$i]['kodikos']))
                <?php $count++; ?>
                @else 
                @break;
                @endif
                @endfor
                @if (empty($row[$loop->index+$count]['kodikos'] && !$loop->last))
                <td rowspan="{{$count+1}}"><span>{{ $r['kodikos'] }} </span></td>
                <td rowspan="{{$count+1}}"><span>{{ $r['eidos'] }} </span></td>
                @endif
                {{-- Row without children --}}
                @elseif (!empty($r['kodikos']) && !empty($row[$loop->index+1]) && !$loop->last) 
                <td>{{ $r['kodikos'] }} </td>
                <td>{{ $r['eidos'] }} </td>
                @else
                <td style="display: none;"></td><td style="display: none;"></td>
                @endif
                <td>{{ $r['temaxia'] }} </td>
                <td>@if ($r['ekptosi'] > 0 && $r['ekptosi'] <= 1) 
                  {{$r['ekptosi']*100 }}%
                  @else 
                  {{ $r['ekptosi'] }}
                  @endif
                </td>
                <td>{{ $r['xondriki_timi'] }}@if (!empty($r['xondriki_timi']))&euro; @endif</td>
                <td>{{ $r['posotita'] }} </td>
              </tr>
              @php
              if (!empty($r['temaxia']))
                $temp1 = 1;
              if (!empty($r['ekptosi']))
                $temp2 = 1;
              if (!empty($r['xondriki_timi']))
                $temp3 = 1;
              if (!empty($r['posotita']))
                $temp4 = 1;
              @endphp
              @endforeach
@endsection