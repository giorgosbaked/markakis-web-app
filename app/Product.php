<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $table = 'products';
    protected $fillable = [
        'kodikos', 'eidos', 'temaxia', 'ekptosi','xondriki_timi','posotita'
    ];
}
