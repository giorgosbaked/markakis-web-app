<?php

namespace App\Imports;

use App\Product;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Excel;

class ProductsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        /* return new Product([
            'kodikos'     => $row["Κωδικός"],
            'eidos'    => $row["Είδος"], 
            'temaxia' => $row[2],
            'ekptosi'    => $row[3], 
            'xondriki_timi'    => $row[4], 
            'posotita'    => $row[5], 
        ]); */
        return new Product([
            'kodikos'     => $row["kodikos"],
            'eidos'    => $row["eidos"], 
            'temaxia' => $row["temaxia"],
            'ekptosi'    => $row["ekptosi"], 
            'xondriki_timi'    => $row["xondriki_timi"], 
            'posotita'    => $row["posotita"], 
        ]);
    }

    public function temp() {
        $array = array();
        $array = Excel::toArray($this, request()->file('file'));
        return $array;
    }
}
