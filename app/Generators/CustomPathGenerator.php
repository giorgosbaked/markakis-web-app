<?php 

namespace App\Generators;

use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\PathGenerator\PathGenerator;

class CustomPathGenerator implements PathGenerator
{
    public function getPath(Media $media) : string
    {
         /*return $media->model_id.'/asd/';*/
        return '/general/';
        /*return md5($media->id).'/mpampies/';*/
    }
    public function getPathForConversions(Media $media) : string
    {
        return '/general/conv/';
    }
    public function getPathForResponsiveImages(Media $media): string
    {
        return '/general/respo/';
    }
}