<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Input;
use Form;

class GalleryController extends Controller
{
    public function deleteImage($avatar) {
        $avatars = auth()->user()->getMedia('avatar');
        $avatars[$avatar]->delete();
        return redirect()->back();
    }

    public function display() {
        $data = Input::get('agree');
        if ($data) {
            $avatar = User::find(3);
            $avatars = $avatar->getMedia('avatar');
            $temp = array();
            foreach ($data as $big_name){
                array_push($temp, $avatars->where('id', $big_name)->first());
            }
            $avatars = $temp;
            $pdf = PDF::loadView('pdf',compact('avatars'))->setPaper('a4', 'landscape');
            return $pdf->stream('labels.pdf');
        } else {
            return redirect()->back();
        }
    }
}
