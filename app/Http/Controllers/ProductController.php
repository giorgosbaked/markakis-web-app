<?php

namespace App\Http\Controllers;

use PDF;
use Excel;
use App\Imports\ProductsImport;
use App\Item;
use DB;
use Illuminate\Support\Facades\Input;

class ProductController extends Controller
{
    public function export_pdf()
    {        
        $array1 = '';
        $array2 = '';
        $array3 = '';
        
        if (request()->file('file1') && request()->input('parafarmaka')) {
            $array1 = Excel::toArray(new ProductsImport, request()->file('file1'));
        } 
        if (request()->file('file2') && request()->input('otc')) {
            $array2 = Excel::toArray(new ProductsImport, request()->file('file2'));
        } 
        if (request()->file('file3') && request()->input('farmaka')) {
            $array3 = Excel::toArray(new ProductsImport, request()->file('file3'));
        }
        
        $input = Input::only('cover_date','extra_page','title1','subtitle1','title2','subtitle2','title3','subtitle3','editor1','editor2','editor3','quantity1','quantity2','quantity3');

        return view('pdf', compact('array1','array2','array3','input'));
    }
}
